using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour { 
    [SerializeField]
    private GameObject enemy = null;
    [SerializeField]
    private Transform startPos = null;
    [SerializeField]
    private Transform endPos = null;

    private int maxEnemyCnt = 15;
    private int EnemyCnt = 0;

    private float enemySpawnTimer = 0;
    private float enemySpawnDelay = 2f;

    private bool enableSpawn = true;
    
    

    private void Awake() {
        

    }

    private void Update() {
        if (!enableSpawn) SpawnFunc();
        else if (enableSpawn) SpawnEnemy();
    }



    public void SpawnEnemy() {
        GameObject obj = Instantiate(enemy, startPos.position, Quaternion.identity);
        obj.transform.parent = this.transform;
       
        EnemyCnt++;
        enableSpawn = false;
        
    }

    private void SpawnFunc() {
        enemySpawnTimer += Time.deltaTime;
        if (enemySpawnTimer >= enemySpawnDelay) {
            enemySpawnTimer = 0;
            enableSpawn = true;
        }
    }
}
