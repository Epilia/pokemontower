using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileManager : MonoBehaviour {
    private Transform[] trs = null;
    public List<Transform> trList = new List<Transform>();

    private int listEndIdx;

    private void Awake() {
        trs = GetComponentsInChildren<Transform>();
        foreach (Transform tr in trs) {
            if (tr == this.transform) continue;
            trList.Add(tr);
            
        }

        listEndIdx = trList.Count - 1;

        for(int i = 0; i < listEndIdx; i++) {
            trList[i].GetComponent<TileTest>().setNextTarget(trList[i + 1]);
            trList[i].name += i.ToString();
        }
        trList[listEndIdx].GetComponent<TileTest>().setNextTarget(trList[listEndIdx]);
        
    }
    

}
