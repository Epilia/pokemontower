using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MousePoint : MonoBehaviour {
    
    void Update()
    {

        // left click - get info from selected tile
        if (Input.GetMouseButtonDown(0))
        {
            // get mouse click's position in 2d plane
            Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            RaycastHit2D hit = Physics2D.Raycast(mousePos, Vector2.zero);
            if (hit) {
                Debug.Log(hit.collider.name);
            }
            // convert mouse click's position to Grid position
            //GridLayout gridLayout = transform.parent.GetComponentInParent<GridLayout>();
            //Vector3Int cellPosition = gridLayout.WorldToCell(mousePos);
            ////gridLayout.GetComponent<Coli>
            //Debug.Log(cellPosition);

        }
    }
}
