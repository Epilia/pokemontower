using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
public class TileTest : MonoBehaviour {

    [SerializeField]
    private Transform target;

    private void Awake() {
        target = this.transform;
    }

    public void setNextTarget(Transform _target) {
        target = _target;
    }
    public Transform getTarget() {
        return target;
    }
}

