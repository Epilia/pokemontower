using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyTest : MonoBehaviour
{

    [SerializeField]
    private Vector3     dir = Vector3.right;
    [SerializeField]
    private Transform   target = null;
    [SerializeField]
    private Transform   nextTarget = null;

    private float       moveSpeed = 1f;
    private bool        isArrived = false;

    
    private void Awake() {

    }
    
    private void Update() {

        float dist = Vector3.Distance(target.position, this.transform.position);
        //TODO : target �ڲ� null��� ����!!!
        if (dist <= 0.1f) { isArrived = true; }
        if (isArrived) {
            setDirection();
        }
            
        MoveToDir();
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.gameObject.tag == "EnemyRoad") {

            target = collision.transform;
            nextTarget = collision.GetComponent<TileTest>().getTarget();

        }
    }
    
    
    private void setDirection() {
        target = nextTarget;
        dir = (target.position - this.transform.position);
        dir.x = Mathf.Round(dir.x);
        dir.y = Mathf.Round(dir.y);
        dir = dir.normalized;
        isArrived = false;

    }

    private void MoveToDir() {

        this.transform.position += dir * moveSpeed * Time.deltaTime;
    }
}
