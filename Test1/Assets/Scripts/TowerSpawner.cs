using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerSpawner : MonoBehaviour
{
    
    [SerializeField]
    private GameObject[] towerPrefab;   // 배열로 수정

    [SerializeField]
    EnemySpawner enemySpawner; // 현재 맵의 적 리스트 정보 얻기
    [SerializeField]
    private int towerBuildGold = 50; // 타워 건설에 사용되는 골드
    [SerializeField]
    private PlayerGold playerGold; // 타워 건설시 골드 감소를 위해 사용


    private void Awake() {
        towerPrefab = Resources.LoadAll<GameObject>("Towers"); // Resources의 Towers폴더에서 타워프리팹 전체 로드   
        
    }


    public void SpawnTower(Transform tileTransform)
    {   // 타워 건설 가능 여부 확인
        if (towerBuildGold > playerGold.CurrentGold) 
        {
            return;
        }

        Tile tile = tileTransform.GetComponent<Tile>();

        
        if (tile.IsBuildTower == true)
        {   // 1. 현재 타일의 위치에 이미 타워가 있다면 건설x
            return;
        }

        tile.IsBuildTower = true;
        // 타워가 건설되어 있음으로 설정

        playerGold.CurrentGold -= towerBuildGold; 
        // 건설에 필요한 비용만큼 골드감소
        
        GameObject clone = Instantiate(towerPrefab[0], tileTransform.position, Quaternion.identity);
        // 선택한 타일의 위치에 타워 건설

        clone.GetComponent<myTower>().SetEnemies(enemySpawner);
        clone.transform.parent = this.gameObject.transform;
        //clone.GetComponent<TowerWeapon>().Setup(enemySpawner);
        // 타워 무기에 에너미 스포너 정보 전달
    }

}
