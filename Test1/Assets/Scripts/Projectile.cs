using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    private Movement2D movement2D;
    private Transform target;
    private int damage;

    public void Setup(Transform target, int damage)
    {
        movement2D = GetComponent<Movement2D>();
        this.target = target; // 타워가 설정해둔 타켓
        this.damage = damage; // (추가됨) 공격력
    }

    private void Update()
    {
        if (target != null) // 타겟 존재시
        {
            // 투사체를 타겟 위치로 쏘기
            Vector3 direction = (target.position - transform.position).normalized;
            movement2D.MoveTo(direction);
        }
        else // 타겟 없을시 삭제
        {
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!collision.CompareTag("Enemy")) return; // 적이 아니면 리턴
        if (collision.transform != target) return; // 현재 타켓 아니면 리턴

        collision.GetComponent<EnemyHP>().TakeDamage(damage); // (변경됨)
        Destroy(gameObject);
    }


}
