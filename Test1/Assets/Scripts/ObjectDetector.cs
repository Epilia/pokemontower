using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectDetector : MonoBehaviour
{
    [SerializeField] 
    private TowerSpawner towerSpawner;
    [SerializeField]
    private myTower selectedTower;

    private Camera mainCamera;
    private Ray ray;
    private RaycastHit hit;

    private void Awake()
    {
        mainCamera = Camera.main;
        //메인카메라 태그를 가지고 있는 오브젝트를 탐색후 카메라 컴포넌트 정보전달
    }

    private void Update() {
        if (Input.GetMouseButtonDown(0)) {
            if (selectedTower) { 
                selectedTower.gameObject.transform.GetChild(0).gameObject.SetActive(false);

            }

            ray = mainCamera.ScreenPointToRay(Input.mousePosition);

            //카메라의 위치에서 화면의 마우스 위치를 관통하는 광선 생성

            //if (Physics.Raycast(ray, out hit, Mathf.Infinity)) {
            //        // 광선에 부딪친 오브젝트 태그가 Tile이면
            //    if (hit.transform.CompareTag("Tile")) {
            //        towerSpawner.SpawnTower(hit.transform);
            //        // 타워를 생성하는 SpawnTower 호출
            //    }
            //}

            Vector3 camPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            RaycastHit2D hit = Physics2D.Raycast(camPos, Vector2.zero);

            string colliderTag = hit.collider.tag;

            if (colliderTag == "Tile") {
                towerSpawner.SpawnTower(hit.transform);
            }

            if (colliderTag == "Tower") {
                // 타워 업그레이드
                selectedTower = hit.collider.GetComponent<myTower>();
                selectedTower.StartCoroutine("DrawRange");

            }

        }
        if (Input.GetKeyDown(KeyCode.Escape)) {
            if (selectedTower) {
                selectedTower.gameObject.transform.GetChild(0).gameObject.SetActive(false);
                selectedTower = null;

            }
        }
    }

}






