using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField] private GameObject enemyPrefab; // 적 프리팹
    [SerializeField] private GameObject enemyHPSliderPrefabs; // 적 체력을 나타내는 UI 프리팹 (추가됨)
    [SerializeField] private Transform canvasTransform; // ui를 표현하는 캔버스 오브젝트의 트랜스폼 (추가됨)
    [SerializeField] private float spawnTime; // 적 생성 주기
    [SerializeField] private Transform[] wayPoints; // 현재 스테이지의 이동 경로
    [SerializeField] private PlayerHP playerHP; // 플레이어 체력 컴포넌트 (추가됨) 
    [SerializeField] private PlayerGold playerGold; // (추가됨)
    private List<Enemy> enemyList;

    private int enemyCount;
    private int enemyMaxCount;

    public List<Enemy> EnemyList => enemyList;

    private void Awake()
    {
        spawnTime = 1.0f;

        //추가
        enemyCount = 0;
        enemyMaxCount = 15;
        //

        enemyList = new List<Enemy>();
        // 적 리스트 메모리 할당

        StartCoroutine("SpawnEnemy");
        // 적 생성 코루틴 함수 호출
    }

    private void Update()
    {
        //추가
        if (enemyCount >= enemyMaxCount) StopCoroutine("SpawnEnemy");
        //
    }

    private IEnumerator SpawnEnemy()
    {
        while (true)
        {
            GameObject clone = Instantiate(enemyPrefab); // 적 오브젝트 생성
            Enemy enemy = clone.GetComponent<Enemy>(); // 방금 생선된 적의 Enemy 컴포넌트는

            enemy.Setup(this, wayPoints); // wayPoint 정보를 매개변수로 Setup() 호출
            enemyList.Add(enemy); // 리스트에 방금 생성된 정보를 저장

            //추가
            clone.transform.name += enemyCount.ToString();
            enemyCount++;
            //추가

            SpawnEnemyHPSlider(clone); // (추가됨) 적 체력을 나타내는 ui 생성 및 설정

            yield return new WaitForSeconds(spawnTime); // 스폰 타임 동안 대기
        }
    
    }

    public void DestoryEnemy(EnenmyDestoryType type, Enemy enemy, int gold) // (변경됨)
    {
        if (type == EnenmyDestoryType.Arrive) // (추가됨)
        {   // 적이 목표지점에 도착했을때
            playerHP.TakeDamage(1); // (추가됨)
        }
        else if (type == EnenmyDestoryType.Kill) // (추가됨)
        {
            playerGold.CurrentGold += gold; // (추가됨)
        }
         
        enemyList.Remove(enemy);
        // 리스트에서 사망하는 적 정보 삭제

        Destroy(enemy.gameObject);
        // 적 오브젝트 삭제
    }

    private void SpawnEnemyHPSlider(GameObject enemy) // (추가됨)
    {
        GameObject sliderClone = Instantiate(enemyHPSliderPrefabs);
        // 적 체력 나타내는 ui 생성
        sliderClone.transform.SetParent(canvasTransform);
        // 슬라이더 ui 오브젝트를 캔버스의 자식으로 설정  
        sliderClone.transform.localScale = Vector3.one;
        // 계층 설정으로 바뀐 크기를 다시 1,1,1로 바꾸기
        sliderClone.GetComponent<SliderPositionAutoSetter>().Setup(enemy.transform);
        // UI가 쫓아다닐 대상을 본인으로 설정
        sliderClone.GetComponent<EnemyHPviewer>().Setup(enemy.GetComponent<EnemyHP>());
        // UI에 자신의 체력 정보를 표시
    }



}
