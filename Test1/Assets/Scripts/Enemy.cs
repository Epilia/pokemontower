using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EnenmyDestoryType {Kill = 0, Arrive} // (추가됨)

public class Enemy : MonoBehaviour
{
    private int wayPointCount; // 이동경로 개수
    private Transform[] wayPoints; // 이동경로 정보
    private int currentIndex = 0; // 현재 목표지점 인덱스
    private Movement2D movement2D; // 오브젝트의 이동제어
    private EnemySpawner enemySpawner; // 적 삭제를 에너미 스포너에 알려서 삭제
    [SerializeField] private int gold = 10; // (추가됨) 적 사망시 골드 획득량

    public void Setup(EnemySpawner enemySpawner, Transform[] wayPoints)
    {
        movement2D = GetComponent<Movement2D>();
        // 적 이동 경로 WayPoints 정보 설정
        this.enemySpawner = enemySpawner;

        wayPointCount = wayPoints.Length;
        this.wayPoints = new Transform[wayPointCount];
        this.wayPoints = wayPoints;

        transform.position = wayPoints[currentIndex].position;
        // 적의 위치를 첫번째 wayPoint(스타트) 지점으로 설정

        StartCoroutine("OnMove");
        // 적 이동 코루틴 함수 시작
    }

    private IEnumerator OnMove()
    {
        NextMoveTo();

        while (true)
        {
            transform.Rotate(Vector3.forward * 10);
            // 적들이 회전하면서 이동

            if (Vector3.Distance(transform.position, wayPoints[currentIndex].position) < 0.02f * movement2D.MoveSpeed)
            {   // 탈주 방지
                NextMoveTo();
                // 다음 이동 방향 설정
            }
            yield return null;

        }
    }

    private void NextMoveTo()
    {
        if (currentIndex < wayPointCount - 1)
        // 아직 이동할 wayPoint가 남았으면
        {
            transform.position = wayPoints[currentIndex].position;
            currentIndex++;
            // 다음 목표 지점으로 이동
            Vector3 direction = (wayPoints[currentIndex].position - transform.position).normalized;
            movement2D.MoveTo(direction); // 내가 이거를 안 넣어서 몹이 안움직였는데 나중에 원 스크립트랑 비교해보기
        }
        else
        {
            gold = 0; // (추가됨)
            onDie(EnenmyDestoryType.Arrive); // (변경됨)
        }
    }

    public void onDie(EnenmyDestoryType type) // (변경됨)
    {
        enemySpawner.DestoryEnemy(type, this, gold); //(변경됨)

    }


}
