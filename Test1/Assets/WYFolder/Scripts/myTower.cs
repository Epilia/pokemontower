using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class myTower : MonoBehaviour {
    private enum TowerClass { 
        NONE = 1,
        EVOLVED,
        FINAL_EVOLVED
    }
    
    public EnemySpawner enemySpawner;

    [SerializeField]
    private GameObject bullet;
    [SerializeField]
    private GameObject rangeSprite;
    [SerializeField]
    private Transform closestTarget;
    
    private float atkRange;
    private float atkDelay;
    private int atk;

    private TowerClass towerClass;

    [SerializeField]
    private bool enableAtk;
    [SerializeField]
    private bool isTargetInRange;

    private void Awake() {
        atkRange = 1.5f;
        atkDelay = 0.7f;
        atk = 2;

        rangeSprite = this.gameObject.transform.GetChild(0).gameObject;
        rangeSprite.transform.localScale = new Vector3(atkRange * 2, atkRange * 2);
        Color rsColor = rangeSprite.GetComponent<SpriteRenderer>().color;
        rsColor.a = 0.5f;
        
        rangeSprite.GetComponent<SpriteRenderer>().color = rsColor;
        rangeSprite.SetActive(false);

        towerClass = TowerClass.NONE;

        enableAtk = true;
        isTargetInRange = false;
    }

    private void Update()
    {
        //if(!isTargetInRange)
        //SearchTarget();
        //if (closestTarget) {
        //    RangeFunc();
        //    if (enableAtk && isTargetInRange) {
        //        StartCoroutine("AtkFunc");
        //        
        //    }
        //}

        if (!closestTarget || !isTargetInRange) SearchTarget();
        if (closestTarget) RangeFunc();
        if (enableAtk && isTargetInRange) StartCoroutine("AtkFunc");
    }

    IEnumerator AtkFunc() {
        enableAtk = false;
        GameObject obj = Instantiate(bullet, this.transform);
        Vector3 dir = closestTarget.position - this.transform.position;
        dir.Normalize();
        obj.GetComponent<myBullet>().SetUp(dir, atk, atkRange);

        yield return new WaitForSeconds(atkDelay);
        enableAtk = true;

    }

    public void SetEnemies(EnemySpawner _enemySpawner) {
        enemySpawner = _enemySpawner;
    }

    private void RangeFunc() {
        if (!closestTarget) { 
            isTargetInRange = false;
            return;
        }

        float distFromClosestTarget = Vector3.Distance(closestTarget.position, this.transform.position);
        if (distFromClosestTarget <= atkRange) {
            isTargetInRange = true;
        }
        else if (distFromClosestTarget > atkRange) {
            isTargetInRange = false;
        }

    }


    private void SearchTarget() {
        if (enemySpawner.EnemyList.Count == 0) return;
        for (int i = 0; i < enemySpawner.EnemyList.Count; i++)
        {
            Transform target = enemySpawner.EnemyList[i].transform;

            if (closestTarget == null)
            {
                closestTarget = target;

                return;
            }

            float distFromClosestTarget = Vector3.Distance(closestTarget.position, this.transform.position);
            float distFromTarget = Vector3.Distance(target.position, this.transform.position);

            if (distFromTarget < distFromClosestTarget)
            {
                closestTarget = target;

            }

        }
        
    }

    private void SearchTargetTest() {
        if (enemySpawner.EnemyList.Count == 0) return;
        for (int i = 0; i < enemySpawner.EnemyList.Count; i++) {
            Transform target = enemySpawner.EnemyList[i].transform;

            if(closestTarget == null) {
                closestTarget = target;
                return;
            }
            float distFromClosestTarget = Vector3.Distance(closestTarget.position, this.transform.position); 
            float distFromTarget = Vector3.Distance(target.position, this.transform.position);

            if (distFromTarget < distFromClosestTarget)
            {
                closestTarget = target;

            }

        }

    }

    IEnumerator DrawRange() {
        rangeSprite.SetActive(true);
        yield return null;
    }

    public void TowerUpgrade() {
        if (towerClass == TowerClass.FINAL_EVOLVED) {
            Debug.Log("this tower is final evoltion state");
            return;
        }
        towerClass++;
        //이미지 변경

        // 스탯변경
        //공격력,
    }
    
}
