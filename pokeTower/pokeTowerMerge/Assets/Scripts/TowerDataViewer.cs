using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TowerDataViewer : MonoBehaviour
{
    [SerializeField] private Image imageTower;
    [SerializeField] private TextMeshProUGUI textDamege;
    [SerializeField] private TextMeshProUGUI textRate;
    [SerializeField] private TextMeshProUGUI textRange;
    [SerializeField] private TextMeshProUGUI textLevel;
    [SerializeField] private Button buttonUpgrade; // 7강 추가됨
    private myTower currentTower;


    private void Awake()
    {
        OffPanel();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            OffPanel();
        }
    }

    public void OnPanel(Transform myTower)
    { // 타워 정보창 켜기
        currentTower = myTower.GetComponent<myTower>(); // 출력해야하는 타워 정보를 받아오기
        gameObject.SetActive(true);
        
        UpdateTowerData(); // 타워정보 갱신 
    }

    public void OffPanel()
    { // 타워 정보창 끄기
        gameObject.SetActive(false);
        


    }

    private void UpdateTowerData()
    {
        textDamege.text = "Damage : " + currentTower.Atk;
        textRate.text = "Rate : " + currentTower.Rate;
        textRange.text = "Range : " + currentTower.Range;
        textLevel.text = "Level " + currentTower.Level;
    }

    public void OnClickEventUpgrade() // (7강 추가) 업글버튼 누르면 해당 함수를 호출 => 업글된 타워로 정보갱신
    {
        currentTower.TowerUpgrade();
        UpdateTowerData(); // 타워를 업그레이드 하면 타워 정보갱신

    }

    public void OnClickEventTowerSell() // (7강 추가) 판매버튼 누르면 해당 함수를 호출
    {
        currentTower.Sell(); // Sell 함수 불러와서 타워제거
        OffPanel(); // 타워가 판매되었으니 패널창 off      
    }
}
