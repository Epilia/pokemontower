using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHP : MonoBehaviour
{
    [SerializeField] private float maxHP =3; // 최대 체력
    private float currentHP; // 현재 체력
    private bool isDie = false; // 적이 사망 해부리면 isDie가 true로 변경됨
    private Enemy enemy;
    private SpriteRenderer spriteRenderer;

    public float MaxHP => maxHP;
    public float CurrentHP => currentHP;

    private void Awake()
    {
        currentHP = maxHP; // 현재 체력을 최대 체력으로 설정
        enemy = GetComponent<Enemy>();
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    public void TakeDamage(float damage)
    {
        if (isDie == true) return;
        currentHP -= damage;

        StopCoroutine("HitAlphaAnimation");
        StartCoroutine("HitAlphaAnimation"); // 맞으면 투명하게 해주는듯

        if (currentHP <= 0)
        {
            isDie = true;
            enemy.onDie(EnenmyDestoryType.Kill); // 변경됨
        }

    }

    private IEnumerator HitAlphaAnimation()
    {
        Color color = spriteRenderer.color; // 적 색상을 컬러 변수에 저장
        color.a = 0.4f; // 맞으면 투명도 낮추고
        spriteRenderer.color = color;

        yield return new WaitForSeconds(0.05f);
        color.a = 1.0f; // 다시 정상 색상으로 복귀
        spriteRenderer.color = color;

    }



}
