using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TextTMPviewer : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI textPlayerHP; 
    [SerializeField] private PlayerHP playerHP;
    [SerializeField] private TextMeshProUGUI textPlayerGold;
    [SerializeField] private PlayerGold playerGold;

    private void Update()
    {
        textPlayerHP.text = playerHP.CurrentHP + "/" + playerHP.MaxHP;
        textPlayerGold.text = playerGold.CurrentGold.ToString();
    }


}
