using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveSystem : MonoBehaviour
{
    [SerializeField] private Wave[] waves; // 현재 스테이지의 모든 웨이브 정보
    [SerializeField] private EnemySpawner enemySpawner;
    private int currentWaveIndex = -1; // 현재 웨이브 인덱스

    public int CurrentWave => currentWaveIndex + 1; // 시작이 0이니깐 +1부터 시작
    public int MaxWave => waves.Length;
    // 웨이브 정보 출력을 위한 Get 프로퍼티 (현재 웨이브, 총 웨이브)

    public void StartWave()
    {
        if (enemySpawner.EnemyList.Count == 0 && currentWaveIndex < waves.Length - 1)
        { // 현재 맵에 적이 없고, 실행가능한 웨이브가 남아있으면

            currentWaveIndex ++;           
            enemySpawner.StartWave(waves[currentWaveIndex]);
            // 에너미 스포너의 startwave 함수를 호출, 현재 웨이브 정보 제공
        }
    }
      
}

    [System.Serializable] // 구조체, 클래스를 직렬화 하는 명령                          
    public struct Wave
    {
        public float spawnTime; // 현재 웨이브 적 생성주기
        public int maxEnemyCount; // 현재 웨이브 적 등장숫자
        public GameObject[] enemyPrefabs; // 현재 웨이브 적 등장종류
    }



