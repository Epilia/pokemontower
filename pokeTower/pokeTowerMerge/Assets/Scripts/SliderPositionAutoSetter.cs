using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SliderPositionAutoSetter : MonoBehaviour
{
    // 적 체력을 표현해주는 스크립트임

    [SerializeField] private Vector3 distance = Vector3.down * 20.0f;
    private Transform targetTransform;
    private RectTransform rectTransform;

    public void Setup(Transform target)
    {
        targetTransform = target; // UI가 쫓아다닐 타겟을 설정
        rectTransform = GetComponent<RectTransform>(); // 렉트트랜스폼 정보 얻어오기
    }

    private void LateUpdate()
    {
        if (targetTransform == null)
        {   // 적이 파괴되면 따라다니던 UI도 삭제
            Destroy(gameObject);
            return;
        }
            
        Vector3 screenPosition = Camera.main.WorldToScreenPoint(targetTransform.position);
        // 오브젝트 월드 좌표를 기준으로 화면에서 좌표값을 구해서
        rectTransform.position = screenPosition + distance;   
        // 화면내에서 좌표 + distance 만큼 떨어진 위치를 UI 위치로 설정
    }


}
