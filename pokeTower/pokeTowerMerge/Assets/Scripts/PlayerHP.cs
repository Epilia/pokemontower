using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHP : MonoBehaviour
{
    [SerializeField] private float maxHP = 10.0f; // 플레이어 최대 체력
    [SerializeField] private Image imageScreen; // 추가 (체력 감소시 빨간 화면)
    private float currentHP; // 플레이어 현재 체력


    public float MaxHP => maxHP;
    public float CurrentHP => currentHP;

    private void Awake()
    {
        currentHP = maxHP;
    }

    public void TakeDamage(float damage)
    {
        currentHP -= damage;

        StopCoroutine("HItAlphaAnimation");
        StartCoroutine("HItAlphaAnimation");

        if (currentHP <= 0)
        { 
            // 나중에 게임 오버 넣기. 현재는 공란.
        }
    
    }

    private IEnumerator HItAlphaAnimation()
    {
        Color color = imageScreen.color;
        color.a = 0.4f; //(투명도 40%)
        imageScreen.color = color;

        while(color.a >= 0.0f)
        { // 투명도가 0%가 될 때까지 감소
            color.a -= Time.deltaTime;
            imageScreen.color = color;

            yield return null;
                
        }
    }



}
