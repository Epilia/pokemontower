using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum WeaponState {SearchTarget = 0, AttackToTarget} // 타워 2가지 상태

public class TowerWeapon : MonoBehaviour
{
    public GameObject projectilePrefabs; // 발사체 프리팹
    public Transform spawnPoint; // 발사체 생성 위치
    private int attackDamage = 1; // (추가됨) 공격력
    private float attackRate = 0.5f; // 공격 속도
    private float attackRange = 3.3f; // 공격 범위
    private WeaponState weaponState = WeaponState.SearchTarget; // 타워 상태
    private Transform attackTarget = null; // 공격 대상
    private EnemySpawner enemySpawner; // 게임에 존재하는 적 정보 획득

    public void Setup(EnemySpawner enemySpawner)
    {
        this.enemySpawner = enemySpawner;
        ChangeState(WeaponState.SearchTarget);
        // 최초 웨폰상태를 서치타겟으로 설정
        
    }

    public void ChangeState(WeaponState newState)
    {
        StopCoroutine(weaponState.ToString()); // 이전에 재생 중이던 코루틴 상태 종료
        weaponState = newState; // 상태 변경
        StartCoroutine(weaponState.ToString()); // 새로운 코루틴 상태 재생
    }

    public void Update()
    {
        if (attackTarget != null)
        {
            RotateToTarget(); // 타겟이 널이 아니면 적을 바라보도록 설정
        }
    }

    private void RotateToTarget() // 적을 바라보게 하는 함수
    {   // 원점으로 부터 거리와 수평축으로부터의 각도를 위해 위치를 구하는 극 좌표계 이용
        float dx = attackTarget.position.x - transform.position.x;
        float dy = attackTarget.position.y - transform.position.y;
        float degree = Mathf.Atan2(dy, dx) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(0, 0, degree);
    }

    private IEnumerator SearchTarget()
    {
        while (true)
        {
            float closestDisSqr = Mathf.Infinity;
            // 가장 가까운 적을 찾기 위해 최초 거리를 가장 크게 설정

            for (int i = 0; i < enemySpawner.EnemyList.Count; i++)
            { // 에너미리스트에 있는 현재 맵에 존재하는 모든 적을 검사
                float distance = Vector3.Distance(enemySpawner.EnemyList[i].transform.position, transform.position);
                if (distance <= attackRange && distance <= closestDisSqr)
                { // 현재 검사 중인 적이 공격 범위 내에 있고, 검사한 다른 적보다 거리가 가깝고
                    closestDisSqr = distance;
                    attackTarget = enemySpawner.EnemyList[i].transform;
                }
            }
            
            if (attackTarget != null) // 어택타겟이 널이 아닐시 적을 공격하도록 상태 변경
            {
                ChangeState(WeaponState.AttackToTarget);
            }
            yield return null;
        }
    }

    private IEnumerator AttackToTarget()
    {
        while (true)
        {
            if (attackTarget == null)
            { // 1.target이 있는지 검사. (없으면 서치타겟 상태로 변경)
                ChangeState(WeaponState.SearchTarget);
                break;
            }
            float distance = Vector3.Distance(attackTarget.position, transform.position);
            if (distance > attackRange)
            {
                attackTarget = null;
                ChangeState(WeaponState.SearchTarget);
                // 2. target이 공격 범위 안에 있는지 검사(공격 범위를 벗어나면 새로운 적 탐색)
                break;
            }
             
            yield return new WaitForSeconds(attackRate);
            // 3. attackRate 시간만큼 대기

            SpawnProjectile();
            // 4. 공격 (발사체 생성)
        }
    }

    private void SpawnProjectile()
    {
         GameObject clone = Instantiate(projectilePrefabs, spawnPoint.position, Quaternion.identity);
        // 생성된 발사체에게 공격대상 정보 제공
        
        clone.GetComponent<Projectile>().Setup(attackTarget, attackDamage); // (변경됨)
    }
    
}