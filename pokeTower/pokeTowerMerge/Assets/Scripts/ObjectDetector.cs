using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems; // (추가)

public class ObjectDetector : MonoBehaviour
{
    [SerializeField]
    private TowerSpawner towerSpawner;

    [SerializeField]
    private TowerDataViewer towerDataViewer;

    //[SerializeField] private myTower hitTransform;
    [SerializeField]
    private Transform hitTransform = null; // (추가) 마우스를 픽킹하여 선택한 오브젝트를 임시 저장 


    private Camera mainCamera;

    private void Awake()
    {
        mainCamera = Camera.main;
        //메인카메라 태그를 가지고 있는 오브젝트를 탐색후 카메라 컴포넌트 정보전달
    }

    private void Update()
    {

        if (EventSystem.current.IsPointerOverGameObject() == true) // (추가)
                                                                   // 마우스가 ui에 머물러 있을때는 아래 코드가 실행되지 않도록 해,
                                                                   // 타워 업그레이드를 할때는 타워정보가 비활성화 되지 않도록 함
        {
            return;
        }


        if (Input.GetMouseButtonDown(0))
        {

            Vector3 camPos = mainCamera.ScreenToWorldPoint(Input.mousePosition);
            RaycastHit2D hit = Physics2D.Raycast(camPos, Vector2.zero);

            hitTransform = hit.transform;

            if (!hitTransform) return;

            string hitTrTag = hitTransform.tag;

            if (hitTrTag == "Tile")
            {
                towerSpawner.SpawnTower(hitTransform);
            }

            else if (hitTrTag == "Tower")
            {
                towerDataViewer.OnPanel(hitTransform);
                hitTransform.GetChild(0).gameObject.SetActive(true);

            }

        }

        else if (Input.GetMouseButtonUp(0))
        {
            if (!hitTransform) return;
            if (hitTransform == null || hitTransform.CompareTag("Tower") == false)
            {  // 마우스를 눌렀을때 선택한 오브젝트가 없거나 타워거 아니면
                towerDataViewer.OffPanel();
                // 패널 비활성화

            }
            if (hitTransform.tag == "Tower") hitTransform.GetChild(0).gameObject.SetActive(false);
            hitTransform = null;
        }
    }

}





