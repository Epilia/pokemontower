using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class myBullet : MonoBehaviour {

    [SerializeField]
    private Vector3 dir;
    [SerializeField]
    private Vector3 startPos;

    private float speed;
    private float atkRange;
    private float distFromStart;
    private int atk;

    private void Awake() {
        
        this.GetComponent<SpriteRenderer>().sortingOrder = 1;

        startPos = this.transform.position;
        speed = 3f;
        distFromStart = 0f;

    }
    
    private void Update() {
        this.transform.position += dir * speed * Time.deltaTime;

        distFromStart += Time.deltaTime * speed;
        if(distFromStart >= atkRange) { 
            Destroy(this.gameObject);
        }

    }

    public void SetUp(Vector3 _dir, int _atk, float _atkRange) {
        dir = _dir;
        atk = _atk;
        atkRange = _atkRange;
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        if(collision.transform.tag == "Enemy") {
            
            collision.GetComponent<EnemyHP>().TakeDamage(atk);

            //TODO: 추후에 오브젝트 풀링으로 관리
            Destroy(this.gameObject);

        }
    }

    
}
